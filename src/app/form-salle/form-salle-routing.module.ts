import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormSallePage } from './form-salle.page';

const routes: Routes = [
  {
    path: '',
    component: FormSallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormSallePageRoutingModule {}
