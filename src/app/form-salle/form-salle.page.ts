import { Component, OnInit, NgModule } from '@angular/core';
import { SalleService ,Post} from './../services/salle.service';
import { Router ,ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-form-salle',
  templateUrl: './form-salle.page.html',
  styleUrls: ['./form-salle.page.scss'],
})
export class FormSallePage implements OnInit {


  editing=false;
  post: Post = {
      nom:'',
      password:''
  };
  constructor(private salleService: SalleService,
              private router: Router,
              private activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
     if(paramMap.get('salleId')){
       this.editing = true;
        this.salleService
        .getSallesById(paramMap.get('salleId'))
        .subscribe((res) => {
          this.post = res ;
        });
      }
    });
  }

  saveSalle(nom,password){

   this.salleService.createSalles(nom.value,password.value).subscribe(
     res => {
       this.router.navigate(['/salles']);
     },
     err => console.error(err)
     );

  }

  updateSalle(){
    this.salleService.updateSale(this.post.id,{
      nom: this.post.nom,
      password: this.post.password
    }).subscribe(res =>{
      this.router.navigate(['/salles']);
    });
  }

}
