import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormSallePageRoutingModule } from './form-salle-routing.module';

import { FormSallePage } from './form-salle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormSallePageRoutingModule
  ],
  declarations: [FormSallePage]
})
export class FormSallePageModule {}
