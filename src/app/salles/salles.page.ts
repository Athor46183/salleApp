import { Component } from '@angular/core';
import { SalleService } from '../services/salle.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-salles',
  templateUrl: './salles.page.html',
  styleUrls: ['./salles.page.scss'],
})
export class SallesPage  {
 salles: any = [];
  constructor(
    private salleService: SalleService,
    private alertController: AlertController
  ) { }

  loadSalles() {
    this.salleService.getSalles().subscribe((res) => {
      this.salles = res;},
          (err) => console.log(err));
  }


ionViewWillEnter(){
this.loadSalles();
}

async deleteSalle(id){

 const alert = await this.alertController.create({
   header:'Remove',
   subHeader:'Supprimer cette salle ',
   message: 'Voulez vous vraiment supprimer cette salle ?',
   buttons: [{
     text : 'supprimer',
     handler: () => {
      console.log(id);
 this.salleService.deleteSalles(id).subscribe((res) => {
   this.loadSalles();
 },
                                              (err) => console.error(err));
     }
   },'Annuler']
 });

 await alert.present();
}



}
