import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Post {
 id?: string;
 nom: string;
 password: string;
}
@Injectable({
  providedIn: 'root'
})
export class SalleService {


  // eslint-disable-next-line @typescript-eslint/naming-convention
  API = 'http://localhost:1337/salle-reunions';
  constructor(
    private http: HttpClient
  ) { }

  getSalles() {
    return this.http.get(this.API);
  };
  getSallesById(id: string) {
    return this.http.get<Post>(this.API+'/'+id);

  };

  createSalles(nom: string,password: string){
    return this.http.post(this.API, {
      nom,password

    });
  };

  updateSale(id: string,post: Post){
    return this.http.put(this.API+'/'+id,post);
  }

  deleteSalles(id: string) {
    return this.http.delete(this.API+'/'+id);
  }

}
