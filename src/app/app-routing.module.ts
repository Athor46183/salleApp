import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'salles',
    pathMatch: 'full'
  },
  {
    path: 'salles',
    loadChildren: () => import('./salles/salles.module').then( m => m.SallesPageModule)
  },
  {
    path: 'form-salle',
    loadChildren: () => import('./form-salle/form-salle.module').then( m => m.FormSallePageModule)
  },
  {
    path: 'salles/edit/:salleId',
    loadChildren: () => import('./form-salle/form-salle.module').then(m => m.FormSallePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
